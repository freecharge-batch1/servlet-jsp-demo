import com.freecharge.emp.model.Employee;
import com.freecharge.emp.service.EmployeeService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "addemployee", value = "/addemployee")
public class AddEmployeeServlet extends HttpServlet {

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
      //Get the form data
      String name = request.getParameter("name");
      String email = request.getParameter("email");
      String password = request.getParameter("password");
      System.out.println("name : " + name);
      System.out.println("email : " + email);
      System.out.println("password : " + password);
      Employee employee = new Employee(name,email,password);
      EmployeeService employeeService = new EmployeeService();
      Employee createdEmployee = employeeService.addEmployee(employee);

      PrintWriter writer = response.getWriter();
      writer.println("<html>");
      writer.println("<body>");
      writer.println("<h1> Employee added successfully </h1>");
      writer.println("<h2> Name : " + employee.getName() +"</h2>");
      writer.println("<h2> Email : " + employee.getEmail() +"</h2>");
      writer.println("<h2> Password : " + employee.getPassword() +"</h2>");

      writer.close();;

  }

}
