import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "greeting",
            value = "/greeting-hello")
public class GreetingServlet extends HttpServlet {

    public void init(ServletConfig config) throws ServletException{

    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        //get the count from the request
       // int count = (int) request.getAttribute("count");
        String dbconnection = (String) getServletContext().getAttribute("dbconnection");
        PrintWriter writer = response.getWriter();
        writer.println("<html>");
        writer.println("<body>");
        writer.println("<h1>Hello From Greeting Servlet : " + dbconnection+" </h1>" );
     //   count++;
        writer.println("</body>");
        writer.println("</html>");
    }
}
