import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "hello",
        description = "hello servlet",
        urlPatterns = {"/hello", "/greeting"},
        loadOnStartup = 1,
        initParams = {
                @WebInitParam(name = "username", value = "user111"),
                @WebInitParam(name = "password", value = "user@111")
        }
)
public class HelloServlet extends HttpServlet {

    int count = 0;

    public void init(ServletConfig config) throws ServletException {
        System.out.println("init method ->" + count);
        count++;
        String username = config.getInitParameter("username");
        String password = config.getInitParameter("password");
        System.out.println("username " + username);
        System.out.println("password :" + password);
        //getting ServletContext
        ServletContext servletContext = config.getServletContext();
        String dbname = servletContext.getInitParameter("dbname");
        String dbusername = servletContext.getInitParameter("dbusername");
        String dbpassword = servletContext.getInitParameter("dbpassword");

        System.out.println("dbname : " + dbname);
        System.out.println("db username : " + dbusername);
        System.out.println("db password : " + dbpassword);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       // request.getParameter("")
        response.setContentType("text/html");

       // request.getServletContext().getAttribute()
        //create a database connection
        /*
         * application/xml
         * application/json
         * image/png
         */
        System.out.println("Hello From HelloServlet");
        /*
            Exchange the data between components
            1. Request
                    - this data is available only for the current request
         */
        request.setAttribute("count", count);
        getServletContext().setAttribute("dbconnection","dbconnectioobject");

       /* RequestDispatcher dispatcher = request.getRequestDispatcher("/greeting-hello");
        dispatcher.forward(request,response);*/

        response.sendRedirect("greeting-hello");

    }

}
