package com.freecharge.emp.service;

import com.freecharge.emp.exception.EmployeeNotFoundException;
import com.freecharge.emp.model.Employee;

import java.util.Arrays;
import java.util.List;

public class EmployeeService {

    public Employee addEmployee(Employee employee){
        System.out.println("employee added successfully");
        return employee;
    }

    public Employee getEmployeeByName(String name) throws EmployeeNotFoundException {
        List<Employee> empList = Arrays.asList(new Employee("emp1", "emp1@gmail.com", "emp111"),
                new Employee("emp2", "emp2@gmail.com", "emp222"),
                new Employee("emp3", "emp3@gmail.com", "emp333"));
       return empList.stream().filter(emp -> emp.getName().equals(name)).findFirst().orElseThrow(()-> new EmployeeNotFoundException("employee not found with this name"));
    }
}
