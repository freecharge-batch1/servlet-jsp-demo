package com.freecharge.emp;

import com.freecharge.emp.model.Employee;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@WebServlet(name = "employees", value= "/employee")
public class EmployeeServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Employee> empList = Arrays.asList(new Employee("emp1", "emp1@gmail.com", "emp111"),
                new Employee("emp2", "emp2@gmail.com", "emp222"),
                new Employee("emp3", "emp3@gmail.com", "emp333"));

        HttpSession session = req.getSession();
        session.setAttribute("empList", empList);

        RequestDispatcher dispatcher = req.getRequestDispatcher("employees.jsp");
        dispatcher.forward(req,resp);
    }
}
