package com.freecharge.emp.session;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

@WebServlet(name = "sessionservlet", value="/sessionservlet")
public class SessionServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(); //will create a session if not exists
        System.out.println(session.isNew());
        System.out.println(session.getId());
        session.setMaxInactiveInterval(60);

        Date sessionCreatedTime = new Date(session.getCreationTime());
        Date sessionLastAccessTime = new Date(session.getLastAccessedTime());

        req.setAttribute("email", "user111@gmail.com");

        session.setAttribute("username", "user111");
        session.setAttribute("role", "user");

        req.getServletContext().setAttribute("dbname" , "empdb");
        req.getServletContext().setAttribute("dbusername", "admin111");
        req.getServletContext().setAttribute("dbpassword", "admin@111");

        /*
        PrintWriter writer = resp.getWriter();
        writer.println("<html>");
        writer.println("<body>");
        writer.println("<h1> Session Infor </h1>");
        writer.println("<h2> Session Id : " + session.getId() + "</h2>");
        writer.println("<h2> Session Creation Time : " + sessionCreatedTime + "</h2>");
        writer.println("<h2> Session Last Acessed Time : " + sessionLastAccessTime + "</h2>");
        writer.println("<h2> From Session Object UserName : " + session.getAttribute("username")+ "</h2>");
        writer.println("<h2> From Session Object Role : " + session.getAttribute("role") + "</h2>");
        */
        RequestDispatcher dispatcher = req.getRequestDispatcher("index.jsp");
        dispatcher.forward(req,resp);
    }

}
