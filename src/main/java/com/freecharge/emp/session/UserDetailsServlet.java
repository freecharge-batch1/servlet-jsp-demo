package com.freecharge.emp.session;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "userdetailsservlet", value="/user-details")
public class UserDetailsServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
       // System.out.println(session);
       if(session == null){
            System.out.println("inside if");
            resp.sendRedirect("index.jsp");
        }
        String username = (String) session.getAttribute("username");
        String role = (String) session.getAttribute("role");

        System.out.println("username from UserDetailsServlet :" + username);
        System.out.println("Role from UserDetailsServlet : " + role);

        //session.invalidate();
    }
}
