package com.freecharge.emp.listener;

import com.freecharge.emp.db.DbConnection;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class AppContextListener implements ServletContextListener {

    /*
    This method is called when the application is deployed into server
    and when the container creates the ServletContext object
     */
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("business logic executed when the servletcontext object is created");
        DbConnection dbConnection = new DbConnection();
        sce.getServletContext().setAttribute("dbConnection", dbConnection);
    }

    /*
        Undeploy war file from container
        Tomcat is shutdown
     */
    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println("business logic executed when the servletcontext object is destroyed");
        //DbConnection dbConnection = new DbConnection();
        sce.getServletContext().removeAttribute("dbConnection");
    }
}
