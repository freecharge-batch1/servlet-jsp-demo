<html>
<body>
<h2> Scope Data </h2>
<h2> Request Scope Data </h2>
<h3> Email : <%= request.getAttribute("email") %> </h3>

<h2> Session Scope Data </h2>

<h3> UserName : <%= session.getAttribute("username") %> </h3>
<h3> Role : <%= session.getAttribute("role") %> </h3>

<h2> Application Scope Data </h2>
<h3> Db Name : <%= application.getAttribute("dbname") %> </h2>
<h3> Db UseName : <%= application.getAttribute("dbusername") %> </h2>
<h3> Db Password : <%= application.getAttribute("dbpassword") %> </h2>

</body>
</html>