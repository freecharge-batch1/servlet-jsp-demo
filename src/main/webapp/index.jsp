<%@ page isELIgnored="false" %>
<html>
<body>
<h2>Add Emplooyee</h2>
<!--
<form action="addemployee" method="post">
<label for="name">Name</label>
  <input type="text" id="name" name="name"><br><br>
  <label for="email">Email</label>
  <input type="email" id="email" name="email"><br><br>
  <label for="password">password</label>
    <input type="password" id="password" name="password"><br><br>

  <input type="submit" value="Submit">
</form> -->
<a href="home.jsp">home</a>
<h2> Scope Data </h2>
<h2> Request Scope Data </h2>
<!-- <h3> Email : <%= request.getAttribute("email") %> </h3> -->
<h3> Email : ${requestScope.email} </h3>
<h2> Session Scope Data </h2>
<!--
<h3> UserName : <%= session.getAttribute("username") %> </h3>
<h3> Role : <%= session.getAttribute("role") %> </h3>
-->
<h3> UserName : ${sessionScope.username}  </h3>
<h2> Application Scope Data </h2>
<!--<h3> Db Name : <%= application.getAttribute("dbname") %> </h2> -->
<h3> Db Name : ${applicationScope.dbname} </h2>
<h3> Db UseName : <%= application.getAttribute("dbusername") %> </h2>
<h3> Db Password : <%= application.getAttribute("dbpassword") %> </h2>

</body>
</html>
